
#ifndef BUFQUEUE_H_
#define BUFQUEUE_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

struct buf_elem {
    size_t len;
    float* buf;
};

struct buf_queue {
    struct buf_elem elem;
    struct buf_queue* next;
};

void enqueue(struct buf_queue** q, struct buf_elem elem);
struct buf_elem dequeue(struct buf_queue** q);

void free_buf(struct buf_queue* q, bool rec);

void buf_print(struct buf_queue* q);

#endif

