
#ifndef LOOP_H_
#define LOOP_H_

#include <stdbool.h>

#include <GLFW/glfw3.h>

bool init(void);

void kill(void);

void input(GLFWwindow* wnd, bool* run);
void update(void);
void render(void);

#endif

