
#ifndef DEFS_H_
#define DEFS_H_

#define WWIDTH  (0x100)
#define WHEIGHT (0x100)

// 65536 Hz
// -> nonstandard, but more than enough
#define SAMPLE_RATE ((WWIDTH)*(WHEIGHT))
#define CHANNELS (2)

#define COLOUR_MASK GL_RG

#endif

