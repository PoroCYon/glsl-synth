
#ifndef GLSL_H_
#define GLSL_H_

#include <stddef.h>
#include <stdbool.h>

bool ginit(void);
void gkill(void);

void gupdate(void);
bool greload(void);
float* grender(size_t* len);

void grender_bg(void);

#endif

