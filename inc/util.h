
#ifndef UTIL_H_
#define UTIL_H_

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>

#include <GL/gl.h>

static inline void eprintf(const char* fmt, ...) {
    va_list args;

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}

const char* read_full_file(const char* p);

bool print_shader_info_log (GLuint shdr);
bool print_program_info_log(GLuint prgm);

#endif

