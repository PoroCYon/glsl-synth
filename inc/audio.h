
#ifndef AUDIO_H_
#define AUDIO_H_

#include <stddef.h>
#include <stdbool.h>

bool ainit(void);
void akill(void);

void astart(void);

bool agetneeded(void);

void aenq(float* data, size_t len);

#endif

