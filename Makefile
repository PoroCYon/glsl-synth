INC=inc
SRC=src
OBJ=obj
BIN=bin

LIBS=-lGL -lglfw -lGLEW -lpthread -lm -lportaudio -lrtmidi
CFLAGS="-I$(INC)" -std=gnu11

CC ?= clang

default: debug

OUT=$(BIN)/gs

ifeq ($(CC),clang)
	WALL=everything
	WEXTRA=-Wextra -Wno-vla -Wno-format-nonliteral -Wno-gnu-statement-expression \
        -Wno-unused-parameter -Wno-incompatible-pointer-types-discards-qualifiers
else
	WALL=all
	WEXTRA=-Wextra
endif

WARN=-W$(WALL) $(WEXTRA)

all: makeobj $(OUT)

CFLAGS := $(CFLAGS) $(WARN)

$(OUT): $(OBJ)/bufqueue.o $(OBJ)/util.o $(OBJ)/loop.o $(OBJ)/main.o $(OBJ)/glsl.o $(OBJ)/audio.o
	$(CC) -o $@ $^ \
        $(CFLAGS) $(LIBS)

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

debug: CFLAGS += -DDEBUG -g
debug: all

release: CFLAGS += -DRELEASE -O3
release: cleanbin all

makeobj:
	@if ! [ -d "$(BIN)" ]; then	mkdir "$(BIN)"; fi
	@if ! [ -d "$(OBJ)" ]; then	mkdir "$(OBJ)"; fi

cleanbin:
	@if [ -f "$(OUT)" ]; then	rm "$(OUT)"; fi

clean: cleanbin
	-find "$(OBJ)" -type f -name "*.o" | xargs rm -v

.PHONY: all debug release clean makeobj cleanbin default

