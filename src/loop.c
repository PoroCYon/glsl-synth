
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "defs.h"
#include "util.h"
#include "glsl.h"
#include "audio.h"

#include "loop.h"

static bool firstg, firsta;

bool init(void) {
    bool succ = true;

    firsta = firstg = true;

    succ &= ainit();
    succ &= ginit();

    return succ;
}

void input(GLFWwindow* wnd, bool* run) {
    if (glfwGetKey(wnd, GLFW_KEY_ESCAPE) == GLFW_PRESS ||
            glfwGetKey(wnd, GLFW_KEY_Q ) == GLFW_PRESS ||
            glfwGetKey(wnd, GLFW_KEY_A ) == GLFW_PRESS) {
        *run = false;
    }
}
void update(void) {
    gupdate();
}
void render(void) {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    if (agetneeded()) {
        goto RENDER;
    }
    else if (firstg) {
        firstg ^= firstg;
        goto RENDER;
    }
    else if (firsta) {
        astart();
        firsta ^= firsta;
    }

    goto ANYWAY;
RENDER:
    {
        //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //glClear(GL_COLOR_BUFFER_BIT);

        size_t len;
        float* data = grender(&len);

        aenq(data, len);
    }

ANYWAY:
    grender_bg();
}

void kill(void) {
    akill();
    gkill();
}

