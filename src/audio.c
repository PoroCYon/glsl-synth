
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <portaudio.h>

#include "defs.h"
#include "bufqueue.h"
#include "util.h"

#include "audio.h"

#define BUF_SIZE (SAMPLE_RATE>>16)

static PaStream* str;

static volatile struct buf_queue* q;

static bool n;

struct pa_cb_ud {
    float* cbuf;
    float* cpos;
    size_t clen;
};

static struct pa_cb_ud pcu;

static int pa_cb(const void* inp, void* oup, unsigned long frames,
        const PaStreamCallbackTimeInfo* ti, PaStreamCallbackFlags fs,
        void* ud) {
    struct pa_cb_ud* d = (struct pa_cb_ud*)ud;

    size_t l = frames * CHANNELS;

    // warning: spaghetti code ahead
    if (d && d->cbuf && d->clen) {
        memcpy(oup, d->cpos, l * sizeof(float));
        d->cpos += l;

        if (d->cpos >= d->cbuf + d->clen) {
            //n = true;
            free(d->cbuf);

            d->cpos = d->cbuf = NULL;
            d->clen ^= d->clen;
        }

        return paContinue;
    }

    if (!q) {
        goto FALLBACK;
    }

    struct buf_elem r = dequeue(&q);

    if (!r.len || !r.buf) {
        goto FALLBACK;
    }

    memcpy(oup, r.buf, l * sizeof(float));

    // empty -> GENERATE NOW
    if (!q || !q->elem.len || !q->elem.buf) {
        n = true;
    }

    d->cpos = (d->cbuf = r.buf) + l;
    d->clen = r.len;

    return paContinue;

FALLBACK:
    n = true;
    eprintf("pa_cb: fallback\n");
    memset(oup, 0, l * sizeof(float));

    return paContinue;
}

bool ainit(void) {
    str = NULL;
    q   = NULL;
    n   = false;

    pcu = (struct pa_cb_ud){
        .cbuf = NULL,
        .cpos = NULL,
        .clen = 0
    };

    PaError err = Pa_OpenDefaultStream(&str, 0, CHANNELS, paFloat32, SAMPLE_RATE, BUF_SIZE, pa_cb, &pcu);
    if (err != paNoError) {
        return false;
    }

    return true;
}

void astart(void) {
    Pa_StartStream(str);
}

void akill(void) {
    Pa_StopStream (str);
    Pa_CloseStream(str);
    str = NULL;

    free_buf(q, true);
    q = NULL;
}

void aenq(float* data, size_t len) {
    enqueue(&q, (struct buf_elem){
        .len = len,
        .buf = data
    });
}

bool agetneeded(void) {
    bool r = n;
    n ^= n;
    return r;
}

