
#include <stdlib.h>

#include "util.h"

#include "bufqueue.h"

void enqueue(struct buf_queue** q, struct buf_elem elem) {
    if (!q || !elem.buf || !elem.len) { eprintf("enq fail\n"); return; }

    if (!*q) {
        *q = (struct buf_queue*)calloc(1, sizeof(struct buf_queue));

        (*q)->elem = elem;
        (*q)->next = NULL;
    }
    else if (!(*q)->elem.buf || !(*q)->elem.len) {
        (*q)->elem = elem;
        eprintf("enq: reuse!\n");
    }
    else {
        enqueue(&(*q)->next, elem);
    }
}
struct buf_elem dequeue(struct buf_queue** q) {
    struct buf_elem er = (struct buf_elem){
        .len = 0,
        .buf = NULL
    };

    if (!q) { return er; }

    if (!*q) {
        return er;
    }

    struct buf_elem r = (*q)->elem;

    struct buf_queue* qq = *q;

    *q = (*q)->next;

    qq->next = NULL;
    qq->elem = er;
    free(qq);

    return r;
}

void free_buf(struct buf_queue* q, bool rec) {
    if (!q) { return; }

    if (q->next && rec) {
        free_buf(q->next, rec);
        q->next = NULL;
    }

    free(q);
}

void buf_print(struct buf_queue* q) {
    size_t i = 0;

    for (; q; i++, q = q->next) {
        eprintf("%zu: %zu %p -> %p\n", i, q->elem.len, q->elem.buf, q->next);
    }
}

