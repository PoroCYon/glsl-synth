
#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#include <GL/glew.h>

#include "defs.h"
#include "util.h"

#include "glsl.h"

static const float verts[] = {
    -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
    -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
     1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
     1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
};

static const char* fragp = "shdr/frag.glsl";

static GLuint vb, va, fb, prgm, dprgm, noiset;
static GLuint tex[2];

static GLint off; // in seconds
                  // -> 32 bits is enough for live coding sessions

static time_t last_acc;

static GLuint compile_shdr(const char* vp, const char* fp) {
    bool succ = true;

    const char* vsrc = read_full_file(vp);
    const char* fsrc = read_full_file(fp);

    succ &= vsrc != NULL && fsrc != NULL;

    GLuint vs = glCreateShader(GL_VERTEX_SHADER  ),
           fs = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vs, 1, &vsrc, 0);
    glShaderSource(fs, 1, &fsrc, 0);

    glCompileShader(vs);
    glCompileShader(fs);

    succ &= print_shader_info_log(vs);
    succ &= print_shader_info_log(fs);

    GLuint p = glCreateProgram();
    glAttachShader(p, vs);
    glAttachShader(p, fs);
    glLinkProgram(p);

    succ &= print_program_info_log(p);

    glDeleteShader(vs);
    glDeleteShader(fs);

    free(vsrc);
    free(fsrc);

    if (succ) {
        return p;
    }

    glDeleteProgram(p);

    return 0;
}

bool greload(void) {
    bool succ = true;

    GLuint p = compile_shdr("shdr/vert.glsl", fragp);

    succ &= p != 0;

    if (succ) {
        glUseProgram(p);

        GLint prev  = glGetUniformLocation(p, "prev" ),
              noise = glGetUniformLocation(p, "noise");

        //assert(prev >= 0 && noise >= 0);

        glUniform1i(prev , 0);
        glUniform1i(noise, 1);
        glUseProgram(0);

        if (prgm) {
            glDeleteProgram(prgm);
            eprintf("re");
        }
        eprintf("loaded\n");
        prgm = p;
        return true;
    }

    eprintf("(re)load failed\n");
    return false;
}

bool ginit(void) {
    bool succ = true;

    prgm = 0;
    off  = 0;
    succ &= greload();

    float* randd = calloc(WWIDTH * WHEIGHT * 3, sizeof(float));
    static const float m = 1.0f / (float)RAND_MAX;
    for (size_t i = 0; i < WWIDTH * WHEIGHT * 3; ++i) {
        randd[i] = (float)random() * m;
    }

    glGenBuffers(1, &vb);
    glBindBuffer(GL_ARRAY_BUFFER, vb);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 5 * 4, verts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenVertexArrays(1, &va);

    glGenFramebuffers(1, &fb);

    glGenTextures(2, tex);
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB/*COLOUR_MASK*/, WWIDTH, WHEIGHT, 0, GL_RGB/*COLOUR_MASK*/, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, tex[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB/*COLOUR_MASK*/, WWIDTH, WHEIGHT, 0, GL_RGB/*COLOUR_MASK*/, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glGenTextures(1, &noiset);
    glBindTexture(GL_TEXTURE_2D, noiset);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, WWIDTH, WHEIGHT, 0, GL_RGB, GL_FLOAT, randd);

    free(randd);

    glBindTexture(GL_TEXTURE_2D, 0);

    struct timespec ts;
    succ &= clock_gettime(CLOCK_REALTIME, &ts) == 0;
    last_acc = ts.tv_sec;

    succ &= greload();

    dprgm = compile_shdr("shdr/vert.glsl", "shdr/bgim.glsl");

    succ &= dprgm != 0;

    if (succ) {
        glUseProgram(dprgm);

        GLint loc = glGetUniformLocation(dprgm, "tex");
        assert(loc >= 0);

        glUniform1i(loc, 0);

        glUseProgram(0);
    }

    return succ;
}
void gkill(void) {
    glDeleteVertexArrays(1, &va);
    glDeleteBuffers(1, &vb);
    glDeleteProgram(prgm);
    glDeleteFramebuffers(1, &fb);
    glDeleteTextures(2, tex);
    glDeleteTextures(1, &noiset);
}

void gupdate(void) {
    struct stat st;
    stat(fragp, &st);

    time_t lt = st.st_mtime;

    if (lt > last_acc) {
        greload(); // TODO: check retval
        last_acc = lt + 1;
    }
}

static void unorm_to_snorm(float* data, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        float f = data[i];
        //f = (f - 0.5f) * 2.0f;
        f = fmaf(f, 2.0f, -1.0f);
             if (f >  1.0f) { f =  1.0f; }
        else if (f < -1.0f) { f = -1.0f; }
        data[i] = f;
    }
}
float* grender(size_t* tlen) {
    glBindFramebuffer(GL_FRAMEBUFFER, fb);
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex[0], 0);

    assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex[1]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, noiset);

    glBindVertexArray(va);
    glUseProgram(prgm);
    glBindBuffer(GL_ARRAY_BUFFER, vb);

    GLint pos = glGetAttribLocation(prgm, "ipos");

    assert(pos >= 0);

    GLint o = glGetUniformLocation(prgm, "off");
    if (o > -1) {
        glUniform1f(o, (float)off);
    }

    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(0 * sizeof(GLfloat)));

    glEnableVertexAttribArray(pos);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(pos);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    glActiveTexture(GL_TEXTURE0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    off++;
    glFinish(); // NOTE: this is a huge bottleneck

    size_t tl = WWIDTH * WHEIGHT * CHANNELS;
    float* tdata = (float*)calloc(tl, sizeof(float));
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    glGetTexImage(GL_TEXTURE_2D, 0, COLOUR_MASK, GL_FLOAT, tdata);
    glBindTexture(GL_TEXTURE_2D, 0);

    unorm_to_snorm(tdata, tl);
    *tlen = tl;

    write(STDOUT_FILENO, tdata, tl * sizeof(float));

    tex[0] ^= tex[1]; // 0 = 0^1
    tex[1] ^= tex[0]; // 1 = (0^1)^1 = 0
    tex[0] ^= tex[1]; // 0 = (0^1)^0 = 1

    return tdata;
}

void grender_bg(void) {
    glBindBuffer(GL_ARRAY_BUFFER, vb);
    glUseProgram(dprgm);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex[1]);
    glBindVertexArray(va);

    GLint pos = glGetAttribLocation(dprgm, "ipos");
    GLint tc  = glGetAttribLocation(dprgm, "itc" );

    assert(pos >= 0 && tc >= 0);

    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(0 * sizeof(GLfloat)));
    glVertexAttribPointer(tc , 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(3 * sizeof(GLfloat)));

    glEnableVertexAttribArray(pos);
    glEnableVertexAttribArray(tc );

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(tc );
    glDisableVertexAttribArray(pos);

    glBindVertexArray(0);
    glUseProgram(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glFlush();
}

