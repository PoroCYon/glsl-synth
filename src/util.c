
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <GL/glew.h>

#include "util.h"

const char* read_full_file(const char* p) {
    int filed = open(p, O_RDONLY);
    if (filed < 1)
        return NULL;

    struct stat stat;
    if (fstat(filed, &stat)) {
        close(filed);
        return NULL;
    }

    size_t len = (size_t)stat.st_size;

    char* ret = (void*)malloc(len + 1);

    if (read(filed, ret, len) < 1) {
        close(filed);
        free(ret);
        return NULL;
    }

    ret[len] = 0;

    close(filed);

    return ret;
}

bool print_shader_info_log(GLuint shdr) {
    GLint succ;

    glGetShaderiv(shdr, GL_COMPILE_STATUS, &succ);
    if (!succ)
    {
        GLint len;
        glGetShaderiv(shdr, GL_INFO_LOG_LENGTH, &len);
        char log[len + 1];

        eprintf("shader compilation failed\n");

        glGetShaderInfoLog(shdr, len, &len, log);
        eprintf("%s\n", log);

        return false;
    }

    return true;
}
bool print_program_info_log(GLuint prgm) {
    GLint succ;

    glGetProgramiv(prgm, GL_LINK_STATUS, &succ);
    if (!succ)
    {
        GLint len;
        glGetProgramiv(prgm, GL_INFO_LOG_LENGTH, &len);
        char log[len + 1];

        eprintf("program linking failed\n");

        glGetProgramInfoLog(prgm, len, &len, log);
        eprintf("%s\n", log);

        return false;
    }

    return true;
}

