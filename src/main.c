
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <portaudio.h>

#include "defs.h"
#include "util.h"
#include "loop.h"

static void err_cb(int code, const char* msg) {
    eprintf("GLFW error: %i (%X): %s\n", code, code, msg);
}

static void APIENTRY dbg_out(GLenum src, GLenum type, GLuint id, GLenum sev, GLsizei len, const GLchar* msg, const void* ud) {
    if (sev != GL_DEBUG_SEVERITY_HIGH_ARB && sev != GL_DEBUG_SEVERITY_MEDIUM_ARB && sev != GL_DEBUG_SEVERITY_LOW_ARB) {
        return;
    }

    eprintf("GL debug ");
    switch (src) {
        case GL_DEBUG_SOURCE_API_ARB: eprintf("api "); break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB: eprintf("wnd sys "); break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: eprintf("glslc "); break;
        case GL_DEBUG_SOURCE_THIRD_PARTY_ARB: eprintf("3rd "); break;
        case GL_DEBUG_SOURCE_APPLICATION_ARB: eprintf("appl "); break;
        case GL_DEBUG_SOURCE_OTHER_ARB: eprintf("other-s "); break;
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR_ARB: eprintf("err "); break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: eprintf("depr "); break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB: eprintf("undef "); break;
        case GL_DEBUG_TYPE_PORTABILITY_ARB: eprintf("port "); break;
        case GL_DEBUG_TYPE_PERFORMANCE_ARB: eprintf("perf "); break;
        case GL_DEBUG_TYPE_OTHER_ARB: eprintf("other-t "); break;
    }

    switch (sev) {
        case GL_DEBUG_SEVERITY_HIGH_ARB: eprintf("high"); break;
        case GL_DEBUG_SEVERITY_MEDIUM_ARB: eprintf("med"); break;
        case GL_DEBUG_SEVERITY_LOW_ARB: eprintf("low"); break;
        default: eprintf("note"); break;
        //case GL_DEBUG_SEVERITY_NOTIFICATION_ARB: eprintf("note "); break;
    }

    eprintf(": %i (%X) %s: %s\n", id, id, glewGetErrorString(id), msg);

    if (type == GL_DEBUG_TYPE_ERROR_ARB || sev == GL_DEBUG_SEVERITY_HIGH_ARB) {
        asm("int3");
    }
}

int main() {
    PaError perr = Pa_Initialize();
    if (perr != paNoError) {
        eprintf("Pa_Initialize failed: %s\n", Pa_GetErrorText(perr));
        return 1;
    }

    glfwSetErrorCallback(err_cb);

    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_RED_BITS    ,  8);
    glfwWindowHint(GLFW_GREEN_BITS  ,  8);
    glfwWindowHint(GLFW_BLUE_BITS   ,  8);
    glfwWindowHint(GLFW_ALPHA_BITS  ,  8);
    glfwWindowHint(GLFW_DEPTH_BITS  , 24);
    glfwWindowHint(GLFW_STENCIL_BITS,  8);
    glfwWindowHint(GLFW_DOUBLEBUFFER, true);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    GLFWwindow* wnd = glfwCreateWindow(WWIDTH, WHEIGHT, "glsl_synth", NULL, NULL);
    if (!wnd) {
        eprintf("err: couldn't open glfw window\n");
        glfwTerminate();
        Pa_Terminate(); // ignore errs
        return 1;
    }
    glfwMakeContextCurrent(wnd);

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    glGetError();
    if (err != GLEW_OK) {
        eprintf("glew failed: %s\n", glewGetErrorString(err));
        glfwTerminate();
        Pa_Terminate();
    }

    glfwSwapInterval(0); // TODO: 1?

    if (!init()) {
        eprintf("init failed\n");
        goto KILL_ME;
    }

    GLint flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(dbg_out, NULL);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    }

    glViewport(0, 0, WWIDTH, WHEIGHT);
    do {
        glfwPollEvents();

        bool run = true;
        input(wnd, &run);

        if (!run) {
            break;
        }

        update();
        render();

        glfwSwapBuffers(wnd);
    } while (true);

KILL_ME:
    glfwDestroyWindow(wnd);

    kill();

    glfwTerminate();

    Pa_Terminate();

    return 0;
}

