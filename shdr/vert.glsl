#version 430 core

layout (location = 0) in vec3 ipos;
layout (location = 1) in vec2 itc ;

out vec2 otc;

void main() {
    gl_Position = vec4(ipos, 1.0);
    otc = itc;
}

