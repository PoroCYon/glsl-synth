#version 430 core

uniform sampler2D tex;

in vec2 otc;

layout (location = 0) out vec4 col;

void main() {
    // TODO: more interesting colours
    col = vec4(texture(tex, otc).rgb, 1.0f);
}

