#version 430 core

uniform float off;

uniform sampler2D prev ;
uniform sampler2D noise;

in vec2 otc;

layout (location = 0) out vec4 col;

#define PI  (3.141592653589793)
#define TAU (6.283185307179586)

#define SIZE (0x100)
#define SCALE (1.0f/SIZE)
#define RATE (SIZE*SIZE)
#define IRATE (SCALE*SCALE)

#define FILTER_SIZE (3)
#define FILTER_RES (IRATE*0x10)

#define mkUnorm(x) fma(x,0.5f,0.5f)
#define mkSnorm(x) fma(x,2.0f,-1.0f)

#define oscSin(f) -cos(t*TAU*(f))
#define oscSq(f)  sign(sin(t*TAU*(f)))
#define oscNoise mkSnorm(texture(noise,otc))
#define oscSaw(f) mkSnorm(fract(t*(f)*0.5f))
#define lfoRSaw(f) fract(t*(f)*(-1.0f))
#define lfoRSaw3u(f) pow(fract(t*(f)*(-1.0f)),3)
#define lfoRSaw3s(f) abs(mkSnorm(lfoRSaw3u(f)))
#define oscTri(f) mkSnorm(abs(mkSnorm(fract(t*(f)*0.5f-0.25f))))

// TODO: filters (hi, lo, bandp)

vec2 thing(float t) {
    float fmul = pow(2.0f, 1.0f / 12.0f);
    float poff = 0.05f;
    float vmul = 0.95f;

    float r = 0.0f;

    float f = 220.0f;
    float v =   1.0f;
    float p =   0.0f;

    for (int i = 0; i < 0x80; ++i) {
        //p = i * poff;
        f = f * fmul;
        v = v * vmul;

        r += sin(t * f) * v;
    }

    r = mkUnorm(r * 0.25f);

    return vec2(r, r);

    float left  = mkUnorm(r * sin(t            ));
    float right = mkUnorm(r * sin(t + PI * 0.5f));

    return vec2(left, right);
}

float wave(float t) {
    return oscTri(220.0f) * lfoRSaw3u(2.0f);
}

void main() {
    vec2 uv = gl_FragCoord.xy * SCALE; // vec2 uv = otc;
    float t = fma(uv.x, SCALE, uv.y + off);

    float ff = mkUnorm(wave(t));
    vec2 f = vec2(ff);
    float l = f.x, r = f.y;

    col = vec4(l, r, 0.0f, 1.0f);
}

